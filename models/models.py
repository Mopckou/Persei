from __future__ import absolute_import

from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


class Users(db.Model):

    hash = db.Column(db.String(80), primary_key=True)
    name = db.Column(db.String(100))
    api_key = db.Column(db.String(100))
    api_secret = db.Column(db.String(100))
    token = db.Column(db.String(32), index=True, unique=True)
    token_expiration = db.Column(db.DateTime)


class Operations(db.Model):

    id = db.Column(db.Integer, primary_key=True)
