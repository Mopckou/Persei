models:
	pip install --upgrade --extra-index-url https://pkgs.dev.azure.com/ermakova/Exchange/_packaging/ExchangeModels/pypi/simple/ exchangeModels==0.0.4

common:
	pip install --upgrade --extra-index-url https://pkgs.dev.azure.com/ermakova/Exchange/_packaging/ExchangeCommon/pypi/simple/ exchangeCommon==0.0.4


req:
	pip install -r requirements.txt

format:
	black .