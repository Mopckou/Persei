import json

from marshmallow import Schema, fields, validate
from marshmallow.validate import OneOf, Range, Length


class Chat(Schema):

    chat_id = fields.Int(validate=Range(min=1), required=True)


class Registration(Schema):

    user_name = fields.Str(validate=Length(min=1, max=100), required=True)
    api_key = fields.String(validate=Length(min=1, max=100), required=True)
    api_secret = fields.String(validate=Length(min=1, max=100), required=True)


class Registration2(Schema):

    text = fields.Str(validate=Length(min=1, max=100), required=True)


class UserAuth(Schema):

    name = fields.Str(validate=Length(min=1, max=50), required=True)
    password = fields.Str(validate=Length(min=1, max=200), required=True)


class Rabbit(Schema):
    worker_name = fields.Str(validate=Length(min=1, max=50), required=True)
    sleep = fields.Integer(required=True)
