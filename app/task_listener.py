from os import environ as env
from asyncio import get_event_loop
import json
from aiohttp.web import Application
from asyncio import gather, CancelledError
from aio_pika import Message
import asyncio


async def listen(app: Application):
    queue_name = env.get('TASK_QUEUE', 'test_task_queue')
    async with app['rabbit_channel_pool'].acquire() as channel:
        await channel.set_qos(10)

        queue = await channel.declare_queue(
            queue_name, durable=True, auto_delete=False
        )

        async with queue.iterator() as queue_iter:
            async for message in queue_iter:
                print(message.body)

                async with message.process():
                    await process_task(channel, json.loads(message.body), queue_name)
                    # loop = get_event_loop()
                    # loop.create_task(process_task(channel, json.loads(message.body), queue_name))


async def process_task(channel, body, queue):
    await asyncio.sleep(body['sleep'])
    await channel.default_exchange.publish(
        Message(json.dumps(body).encode()), routing_key=queue
    )


async def task_listener(app: Application):
    try:
        while True:
            await listen(app)

    except CancelledError:
        pass


async def start_listener(app: Application):
    listeners_count = env.get('LISTENERS_COUNT', 3)

    tasks = [f(app) for f in [task_listener] * listeners_count]
    app['task_listeners'] = gather(*tasks)


async def cleanup_listener(app: Application):
    try:
        app['task_listeners'].cancel()
        await app['task_listeners']
    except CancelledError:
        pass
