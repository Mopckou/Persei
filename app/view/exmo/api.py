import urllib
import hashlib
import hmac
import time
import requests

__all__ = [
    'ExmoApi',
]


class ExmoApi:

    def __init__(self, api_key, api_secret, api_url='https://api.exmo.com', api_version='v1'):
        self.API_URL = api_url
        self.API_VERSION = api_version
        self.API_KEY = api_key
        self.API_SECRET = bytes(api_secret, encoding='utf-8')

    def sha512(self, data):
        H = hmac.new(key=self.API_SECRET, digestmod=hashlib.sha512)
        H.update(data.encode('utf-8'))
        return H.hexdigest()

    def api_query1(self, api_method, params={}):
        params['nonce'] = int(round(time.time() * 1000))
        params = urllib.parse.urlencode(params)

        sign = self.sha512(params)
        headers = {
            "Content-type": "application/x-www-form-urlencoded",
            "Key": self.API_KEY,
            "Sign": sign
        }

        s = requests.Session()
        r = s.post("https://api.exmo.com" + "/" + self.API_VERSION + "/" + api_method, data=params, headers=headers)
        return r.text

    async def query(self, session, api_method, params={}):
        params['nonce'] = int(round(time.time() * 1000))
        params = urllib.parse.urlencode(params)

        sign = self.sha512(params)
        headers = {
            "Content-type": "application/x-www-form-urlencoded",
            "Key": self.API_KEY,
            "Sign": sign
        }

        url = self.API_URL + "/" + self.API_VERSION + "/" + api_method
        async with session() as client:
            async with client.post(url, data=params, headers=headers) as s:
                return await s.json()

        # TODO: сделать raise?

        #
        # try:
        #     obj = json.loads(response.decode('utf-8'))
        #     if 'error' in obj and obj['error']:
        #         print(obj['error'])
        #         raise sys.exit()
        #     return obj
        # except json.decoder.JSONDecodeError:
        #     print('Error while parsing response:', response)
        #     raise sys.exit()
