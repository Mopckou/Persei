import aiohttp
import hashlib
from config import GLOBAL_SOL, general_semaphore, exmo_session
from ..api import ExmoApi
from app.db import Users


async def fetch_user_by_hash(conn, user_hash):
    return await conn.fetchrow(
        Users.select().where(Users.c.hash == user_hash)
    )


async def fetch_user(conn, name):
    return await conn.fetchrow(
        Users.select().where(Users.c.name == name)
    )


def get_hash(api_key, api_secret):
    api_key, api_secret = api_key.replace('K-', ''), api_secret.replace('S-', '')
    data = api_secret + api_key + GLOBAL_SOL
    return hashlib.md5(data.encode()).hexdigest()


async def add_user(conn, user_hash, name, api_key, api_secret):
    await conn.execute(
        Users.insert().values(hash=user_hash, name=name, api_key=api_key, api_secret=api_secret)
    )


async def del_user(conn, token):
    await conn.execute(Users.delete().where(Users.c.token == token))


async def check_keys(*args):
    async with general_semaphore:
        ans = await ExmoApi(*args).query(
            exmo_session, 'user_info'
        )
        return 'error' not in ans
