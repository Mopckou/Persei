from aio_pika import pool, Message
import json
from os import environ as env
from aiohttp import web
from config import internal_aes_decipher, external_aes_decipher
from aiohttp_apispec import use_kwargs
from app.schema import Registration, Registration2, Rabbit
from app.core.auth import authorization
from .func import add_user, del_user, fetch_user, fetch_user_by_hash, get_hash, check_keys


__all__ = [
    'registration',
    'register2',
    'unregistration'
]


@use_kwargs(Registration(strict=True), locations=['json'])
async def registration(requests: web.Request) -> web.Response:
    name = requests['data']['user_name']
    api_key, api_secret = external_aes_decipher.decrypt(
        requests['data']['api_key'],
        requests['data']['api_secret']
    )
    user_hash = get_hash(api_key, api_secret)

    async with requests.app['pool'].transaction() as conn:
        user_by_hash = await fetch_user_by_hash(conn, user_hash)
        user_by_name = await fetch_user(conn, name)
        if user_by_name:
            return web.json_response(
                status=409,
                text=f'Пользователь с таким именем уже зарегистрирован!'
            )
        if user_by_hash:
            return web.json_response(
                status=409,
                text=f'Пользователь с такими АПИ ключами уже зарегистрирован!'
            )

        result = await check_keys(api_key, api_secret)
        if not result:
            return web.json_response(
                status=400,
                text=f'Неверные API ключи!!'
            )

        api_key, api_secret = internal_aes_decipher.encrypt(api_key, api_secret)
        await add_user(conn, user_hash, name, api_key, api_secret)

    return web.json_response(
        status=200,
        text=f'Пользователь успешно зарегистрирован!'
    )


@use_kwargs(Registration2(strict=True), locations=['json'])
@authorization
async def register2(requests: web.Request) -> web.Response:
    return web.json_response(
        status=200,
        text=requests['data']['text']
    )


@authorization
async def unregistration(requests: web.Request) -> web.Response:
    async with requests.app['pool'].acquire() as conn:
        await del_user(
            conn,
            requests['data']['user']['token']
        )

    return web.json_response()


@use_kwargs(Rabbit(strict=True), locations=['json'])
async def rabbit(requests: web.Request) -> web.Response:
    await publish(
        requests.app['rabbit_channel_pool'], env.get('TASK_QUEUE', 'test_task_queue'), requests['data']['worker_name'], requests['data']['sleep']
    )

    print("New task create!")

    return web.json_response(
        'OK BRO!'
    )


async def publish(channel_pool: pool.Pool, queue: str, text: str, sleep_time: int):
    async with channel_pool.acquire() as channel:
        await channel.default_exchange.publish(
            Message(json.dumps({'task': text, 'sleep': sleep_time}).encode()),
            queue,
        )