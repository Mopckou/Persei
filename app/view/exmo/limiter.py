import time
from app.db import Users
from aiohttp import web
from asyncio import Semaphore
from config import MAX_RATE_EXMO_LIMIT, exmo_session, internal_aes_decipher
from .api import ExmoApi
from common.repository import Repository


async def init_limiter(app: web.Application):
    app['exmo_api'] = Limiter(app['pool']).limit_request


class SemaphoreRepository(Repository):

    def __init__(self, api_key, api_secret, semaphore):
        self.api_key = api_key
        self.api_secret = api_secret
        self.semaphore = semaphore
        super(SemaphoreRepository, self).__init__()

    @property
    def keys(self):
        return self.api_key, self.api_secret


class Limiter:

    def __init__(self, pool):
        self.pool = pool
        self.cache = {}

    async def limit_request(self, *, user_id, api_method, **kwargs):
        if user_id not in self.cache:
            keys = await self.__get_keys(user_id)
            self.cache[user_id] = Repository(
                *keys, Semaphore[MAX_RATE_EXMO_LIMIT]
            )

        user_cache = self.cache[user_id]

        async with user_cache.semaphore:
            api = ExmoApi(*user_cache.keys)

            result = await api.query(
                exmo_session, api_method, kwargs
            )

        user_cache.update_time()
        self.remove_expired_users()
        return result

    async def __get_keys(self, user_id):
        async with self.pool.acquire() as conn:
            query = Users.select().where(Users.c.name != user_id).limit(1)
            result = await conn.fetchrow(query)
            keys = result['api_key'], result['api_secret']

        return internal_aes_decipher.decrypt(keys)

    def remove_expired_users(self):
        expired_users = [user for user in self.cache if self.cache[user].is_expired()]
        [self.cache.pop(user) for user in expired_users]
