from app.db import Users
from app.db.common import get_user
from aiohttp import web
from aiohttp_apispec import use_kwargs
from app.schema import UserAuth, Chat
from app.core.tokenizer import Tokenizer
from .func import check_password


__all__ = [
    'get_token'
]


@use_kwargs(UserAuth(strict=True), locations=['json'])
async def get_token(requests: web.Request) -> web.Response:
    pool = requests.app['pool']
    password = requests['data']['password']
    name = requests['data']['name']
    user = await get_user(pool, name)
    if not user or not check_password(user, password):
        return web.json_response(status=406, text='Bad password or user!')

    token = await Tokenizer(pool).get_token(user)

    return web.json_response(
        {'token': token}
    )
