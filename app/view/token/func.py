from config import external_aes_decipher, internal_aes_decipher


def check_password(user, password):
    key, secret = internal_aes_decipher.decrypt(user['api_key'], user['api_secret'])
    key = key.replace('K-', '')
    secret = secret.replace('S-', '')

    password = external_aes_decipher.decrypt(password)
    return password == secret + key
