import os
import base64
from app.db import Users
from datetime import datetime
from datetime import timedelta


class UserNotFound(Exception):

    def __repr__(self):
        return 'User not found!'


class Tokenizer:

    def __init__(self, pool):
        self.pool = pool

    async def get_token(self, user):
        now = datetime.utcnow()
        if user['token'] and user['token_expiration'] > now + timedelta(seconds=60):
            return user['token']

        token = base64.b64encode(os.urandom(24)).decode('utf-8')
        token_expiration = now + timedelta(minutes=10)
        await self.__update_token(user['name'], token, token_expiration)

        return token

    async def check_token(self, user):
        if user['token'] is None or user['token_expiration'] < datetime.utcnow():
            return False
        return True

    async def revoke_token(self, token):
        user = await self.get_user_by_token(token)
        if not user:
            raise UserNotFound

        token_expiration = datetime.utcnow() - timedelta(seconds=1)
        await self.__update_token(
            user['name'], user['token'], token_expiration
        )

    async def get_user_by_token(self, token):
        async with self.pool.acquire() as conn:
            query = Users.select().where(Users.c.token == token)
            user = await conn.fetchrow(query)

            return user

    async def __update_token(self, name, token, token_expiration):
        async with self.pool.acquire() as conn:
            query = Users.update().where(Users.c.name == name).values(
                token=token, token_expiration=token_expiration
            )
            return await conn.execute(query)
