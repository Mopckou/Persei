import asyncio
from .db.postgres import init_postgres, close_postgres
from .db.rabbitmq import init_rabbit, close_rabbit
from .task_listener import start_listener
from aiohttp_swagger import setup_swagger
from aiohttp_apispec import validation_middleware, setup_aiohttp_apispec
from aiohttp import web
from app.routes import setup_routes


async def swagger(app):
    setup_swagger(
        app, swagger_info=app['swagger_dict'], swagger_url='/debug/api/docs'
    )


def create_app(loop=None) -> web.Application:
    if loop:
        asyncio.set_event_loop(loop)

    app = web.Application()
    setup_routes(app)

    setup_aiohttp_apispec(
        app=app,
        title='API бирж',
        version='0.0.1',
        securityDefinitions={
            'user': {'type': 'apiKey', 'name': 'Authorization', 'in': 'header'}
        },
    )
    app.middlewares.extend([validation_middleware])

    app.on_startup.extend(
        [
            init_postgres,
            init_rabbit,
            start_listener,
            swagger,
        ]
    )

    app.on_cleanup.extend(
        [
            close_postgres,
            close_rabbit
        ]
    )

    return app
