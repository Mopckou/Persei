from common.aes import AESCipher
from asyncio import Semaphore
import aiohttp
from os import environ as env


MAX_RATE_EXMO_LIMIT = env.get('MAX_RATE_EXMO_LIMIT', 10)

POSTGRES_HOST = env['POSTGRES_HOST']
POSTGRES_PORT = env['POSTGRES_PORT']
POSTGRES_USER = env['POSTGRES_USER']
POSTGRES_PASS = env['POSTGRES_PASS']
POSTGRES_DATABASE = env['POSTGRES_DATABASE']

DEBUG = env.get('DEBUG', True)
INTERNAL_KEY = env['INTERNAL_KEY']
EXTERNAL_KEY = env['EXTERNAL_KEY']

exmo_session = aiohttp.ClientSession
internal_aes_decipher = AESCipher(INTERNAL_KEY)
external_aes_decipher = AESCipher(EXTERNAL_KEY)
general_semaphore = Semaphore(MAX_RATE_EXMO_LIMIT)
GLOBAL_SOL = env['GLOBAL_SOL']
